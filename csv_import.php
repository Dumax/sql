<?php
//including the config file
include 'config.php';
$pdo = connect();

$csv_file = $_FILES['csv_file']['tmp_name'];
if (is_file($csv_file)) {
	$input = fopen($csv_file, 'a+');
	// if the csv file contain the table header leave this line
	$row = fgetcsv($input, 1024, ','); // here you got the header
	while ($row = fgetcsv($input, 1024, ',')) {

		$sql = 'INSERT INTO Countries (CountryName)
				SELECT :CountryName
				FROM Countries
				WHERE NOT EXISTS(
				    SELECT CountryName
				    FROM Countries
				    WHERE CountryName = :CountryName
				)
				LIMIT 1;
				INSERT INTO States (CountryId, StateName)
		 		     VALUES (
		 		     	(SELECT CountryId FROM Countries WHERE CountryName=:CountryName),:StateName);';

		$query = $pdo->prepare($sql);

		$query->bindParam(':CountryName', $row[0], PDO::PARAM_STR);
		$query->bindParam(':StateName', $row[1], PDO::PARAM_STR);

		$query->execute();

	}
}

//redirect to the index page
//header('location: index.php');
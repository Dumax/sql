<?php
// including the config file
include 'config.php';
$pdo = connect();
$sql = 'SELECT Countries.CountryId, Countries.CountryName, States.StateId, States.StateName
		FROM Countries
		LEFT JOIN States
		ON Countries.CountryID=States.CountryID';
$query = $pdo->prepare($sql);
$query->execute();
$results = $query->fetchAll();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Import-export</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
	<nav class="navbar navbar-default">
	    <a href="csv_export.php" class="btn btn-info" role="button"><span class="glyphicon glyphicon-export"></span> Export to CSV</a>
		<a href="xls_export.php" class="btn btn-info" role="button"><span class="glyphicon glyphicon-export"></span> Export to XLS</a>

		<button type="button" class="btn btn-success" onclick="show_popup('xls_popup_upload')"><span class="glyphicon glyphicon-import"></span> Import from XLS</button>
		<button type="button" class="btn btn-success" onclick="show_popup('csv_popup_upload')"><span class="glyphicon glyphicon-import"></span> Import from CSV</button>
	</nav>
	<div class="col-xs-12">
		<h2>Countries and regions</h2>
		<div class="table-wrapper">
			<table class="table table-striped">
			<thead>
			  <!-- <tr>
			  	<th>№</th>
			    <th>customerNumber</th>
			    <th>customerName</th>
			    <th>orderNumber</th>
			    <th>status</th>
			  </tr> -->
			  <tr>
			    <!-- <th>CountryId</th> -->
			    <th>CountryName</th>
			    <!-- <th>StateId</th> -->
			    <th>StateName</th>
			  </tr>
			</thead>
			<tbody>
			<?php
foreach ($results as $result) {
	echo "<tr>";
	// echo "<td>",$result['CountryId'],"</td>";
	echo "<td>", $result['CountryName'], "</td>";
	// echo "<td>",$result['StateId'],"</td>";
	echo "<td>", $result['StateName'], "</td>";
	echo "</tr>";
}
?>
			</tbody>
			</table>
		</div>
	</div>
</div>
<!-- The popup for upload a csv file -->
<div id="csv_popup_upload">
    <div class="form_upload">
        <span class="close" onclick="close_popup('csv_popup_upload')">x</span>
        <h2>Upload CSV file</h2>
        <form action="csv_import.php" method="post" enctype="multipart/form-data">
            <input type="file" name="csv_file" id="csv_file" class="file_input">
            <input type="submit" value="Upload file" id="upload_btn">
        </form>
    </div>
</div>
<!-- The popup for upload a xls file -->
<div id="xls_popup_upload">
    <div class="form_upload">
        <span class="close" onclick="close_popup('xls_popup_upload')">x</span>
        <h2>Upload XLS file</h2>
        <form action="xls_import.php" method="post" enctype="multipart/form-data">
            <input type="file" name="xls_file" id="xls_file" class="file_input">
            <input type="submit" value="Upload file" id="upload_btn">
        </form>
    </div>
</div>
<script>
	// show_popup : show the popup
function show_popup(id) {
	// show the popup
	$('#'+id).show();
}

// close_popup : close the popup
function close_popup(id) {
	// hide the popup
	$('#'+id).hide();
}
</script>
</body>
</html>






<!-- <div class="col-md-6">
		<h2>Products</h2>
		<table class="table table-striped">
		<thead>
		  <tr>
		    <th>Model</th>
		    <th>Type</th>
		    <th>Price</th>
		  </tr>
		</thead>
		<tbody>

		<?php
// foreach ($products as $product) {
// 	echo "<tr>";
//     echo "<td>",$product['productName'],"</td>";
//     echo "<td>",$product['productLine'],"</td>";
//     echo "<td>",$product['buyPrice'],"</td>";
//     echo "</tr>";
// }
?>
		</tbody>
		</table>
	</div> -->
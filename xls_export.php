<?php

require_once 'PHPExcel/Classes/PHPExcel.php';
require_once 'PHPExcel/Classes/PHPExcel/IOFactory.php';
require_once 'config.php';

$objPHPExcel = new PHPExcel;

$pdo = connect();

// set headers to force download on xls format
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="file.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

// select all members
$sql = 'SELECT Countries.CountryName, States.StateName
		FROM Countries
		LEFT JOIN States
		ON Countries.CountryID=States.CountryID';
$query = $pdo->prepare($sql);
$query->execute();
$results = $query->fetchAll();

$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', 'CountryName')
	->setCellValue('B1', 'StateName');

$objPHPExcel->getActiveSheet()->getStyle('A1:B1')->getFont()->setBold(true);

$i = 2;

foreach ($results as $result) {
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A' . $i, $result['CountryName'])
		->setCellValue('B' . $i, $result['StateName']);
	$i++;
}

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Янв 05 2016 г., 08:11
-- Версия сервера: 5.6.27-0ubuntu1
-- Версия PHP: 5.6.11-1ubuntu3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `world`
--

-- --------------------------------------------------------

--
-- Структура таблицы `Countries`
--

CREATE TABLE IF NOT EXISTS `Countries` (
  `CountryId` int(11) NOT NULL,
  `CountryName` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Countries`
--

INSERT INTO `Countries` (`CountryId`, `CountryName`) VALUES
(54, 'Ukraine'),
(61, 'Russia'),
(62, 'USA'),
(63, 'Canada');

-- --------------------------------------------------------

--
-- Структура таблицы `States`
--

CREATE TABLE IF NOT EXISTS `States` (
  `StateId` int(11) NOT NULL,
  `CountryId` int(11) NOT NULL,
  `StateName` varchar(60) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `States`
--

INSERT INTO `States` (`StateId`, `CountryId`, `StateName`) VALUES
(54, 54, 'Poltavska obl'),
(65, 61, 'Belgorodska obl'),
(66, 54, 'Kievska obl'),
(67, 62, 'Washington'),
(68, 63, 'Kvebek');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `Countries`
--
ALTER TABLE `Countries`
  ADD PRIMARY KEY (`CountryId`);

--
-- Индексы таблицы `States`
--
ALTER TABLE `States`
  ADD PRIMARY KEY (`StateId`),
  ADD KEY `CountryId` (`CountryId`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `Countries`
--
ALTER TABLE `Countries`
  MODIFY `CountryId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT для таблицы `States`
--
ALTER TABLE `States`
  MODIFY `StateId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=69;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `States`
--
ALTER TABLE `States`
  ADD CONSTRAINT `States_ibfk_1` FOREIGN KEY (`CountryId`) REFERENCES `Countries` (`CountryId`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

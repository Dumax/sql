<?php
// // including the config file
// include('config.php');
// $pdo = connect();

// // set headers to force download on csv format
// header('Content-Type: text/csv; charset=utf-8');
// header('Content-Disposition: attachment; filename=file.csv');

// // we initialize the output with the headers
// $output = "CountryId, CountryName, StateId, StateName\n";
// // select all members
// $sql = 'SELECT Countries.CountryId, Countries.CountryName, States.StateId, States.StateName
// 		FROM Countries
// 		LEFT JOIN States
// 		ON Countries.CountryID=States.CountryID';
// $query = $pdo->prepare($sql);
// $query->execute();
// $results = $query->fetchAll();
// foreach ($results as $result) {
// 	// add new row
//     $output .= $result['CountryId'].",".$result['CountryName'].",".$result['StateId'].",".$result['StateName']."\n";
// }
// // export the output
// echo $output;
// exit;
//

// including the config file
include 'config.php';
$pdo = connect();

// set headers to force download on csv format
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=file.csv');

// we initialize the output with the headers
$output = "CountryName, StateName\n";
// select all members
$sql = 'SELECT Countries.CountryName, States.StateName
		FROM Countries
		LEFT JOIN States
		ON Countries.CountryID=States.CountryID';
$query = $pdo->prepare($sql);
$query->execute();
$results = $query->fetchAll();
foreach ($results as $result) {
	// add new row
	$output .= $result['CountryName'] . "," . $result['StateName'] . "\n";
}
// export the output
echo $output;
exit;
?>